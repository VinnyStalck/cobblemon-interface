<!-- Use this template to ask for support for a mod that is not currently supported. -->

Add support for [Example](https://modrinth.com/mod/example-mod) mod.

/label ~Modded ~Texture

/milestone %"Mod Support"
