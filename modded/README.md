# Cobblemon Interface: Modded

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/resourcepack/cobblemon-interface-modded)
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://curseforge.com/minecraft/texture-packs/cobblemon-interface-modded)

---

A complementary resourcepack to [Cobblemon Interface](https://modrinth.com/project/cobblemon-interface) that makes many mods interface and sounds fit the [Cobblemon](https://modrinth.com/mod/cobblemon) mod aesthetics.

### Mod Support

<details>
<summary>Supported Mods</summary>

| Mod | Cobblemon Interface |
|----:|:--------------------|
| [AppleSkin](https://modrinth.com/mod/appleskin) | >= v0.1 |
| [Advancement Plaques](https://modrinth.com/mod/advancement-plaques) | >= v0.1 |
| [Overflowing Bars](https://modrinth.com/mod/overflowing-bars) | >= v0.8 |
| [Dehydration](https://modrinth.com/mod/dehydration) | >= v0.8.3 |
| [Legendary Tooltips](https://modrinth.com/mod/legendary-tooltips) | >= v0.8.3 |
| [TrashSlot](https://modrinth.com/mod/trashslot) | >= v0.8.4 |
| [Apotheosis](https://www.curseforge.com/minecraft/mc-mods/apotheosis) | >= v0.9, < v1.7 |
| [Automobility](https://modrinth.com/mod/automobility) | >= v0.9 |
| [Better Advancements](https://modrinth.com/mod/better-advancements) | >= v0.9 |
| [Cobblemon Integrations](https://modrinth.com/mod/cobblemon-integrations) | >= v0.9 |
| [Cooking for Blockheads](https://modrinth.com/mod/cooking-for-blockheads) | >= v0.9 |
| [Cosmetic Armor Reworked](https://minecraft.curseforge.com/projects/cosmetic-armor-reworked) | >= v0.9 |
| [Curios API](https://modrinth.com/mod/curios) | >= v0.9 |
| [Easy Villagers](https://modrinth.com/mod/easy-villagers) | >= v0.9 |
| [EMI](https://modrinth.com/mod/emi) | >= v0.9 |
| [Farmer's Delight](https://modrinth.com/mod/farmers-delight) | >= v0.9 |
| [Farmer's Delight [Fabric]](https://modrinth.com/mod/farmers-delight-fabric) | >= v0.9 |
| [Realistic Horse Genetics](https://modrinth.com/mod/realistic-horse-genetics) | >= v0.9 |
| [Iron Chests](https://modrinth.com/mod/iron-chests) | >= v0.9 |
| [Just Enough Items](https://modrinth.com/mod/jei) | >= v0.9 |
| [Just Enough Resources](https://modrinth.com/mod/just-enough-resources-jer) | >= v0.9 |
| [Just Enough Professions](https://modrinth.com/mod/just-enough-professions-jep) | >= v0.9 |
| [Little Logistics](https://modrinth.com/mod/little-logistics) | >= v0.9 |
| [PokeFood](https://modrinth.com/mod/pokefood) | >= v0.9 |
| [Polymorph](https://modrinth.com/mod/polymorph) | >= v0.9 |
| [Rechiseled](https://modrinth.com/mod/rechiseled) | >= v0.9 |
| [Sophisticated Backpacks](https://www.curseforge.com/minecraft/mc-mods/sophisticated-backpacks) | >= v0.9 |
| [Sophisticated Core](https://www.curseforge.com/minecraft/mc-mods/sophisticated-core) | >= v0.9 |
| [Sophisticated Storage](https://www.curseforge.com/minecraft/mc-mods/sophisticated-storage) | >= v0.9 |
| [Xaero's Minimap (Fair-Play)](https://modrinth.com/mod/xaeros-minimap-fair) | >= v0.9 |
| [Simple Voice Chat](https://modrinth.com/plugin/simple-voice-chat) | >= v0.9.1 |
| [Tom's Simple Storage](https://modrinth.com/mod/toms-storage) | >= v0.9.1 |
| [Waystones](https://modrinth.com/mod/waystones) | >= v0.9.1 |
| [Zenith](https://modrinth.com/mod/zenith) | >= v0.9.1, < v1.7 |
| [Create](https://modrinth.com/mod/create) | >= v1.0.0 |
| [Create Fabric](https://modrinth.com/mod/create-fabric) | >= v1.0.0 |
| [Expanded Storage](https://modrinth.com/mod/expanded-storage) | >= v1.1 |
| [Iron Furnaces](https://modrinth.com/mod/iron-furnaces) | >= v1.1 |
| [Trinkets](https://modrinth.com/mod/trinkets) | >= v1.1 |
| [Inmis](https://modrinth.com/mod/inmis) | >= v1.2 |
| [Inventorio](https://modrinth.com/mod/inventorio) | >= v1.2 |
| [Roughly Enough Items (REI)](https://modrinth.com/mod/rei) | >= v1.3 |
| [Traveler's Backpack](https://modrinth.com/mod/travelersbackpack) | >= v1.3 |
| [Applied Energistics 2](https://modrinth.com/mod/ae2) | >= v1.4 |
| [Applied Energistics 2 Wireless Terminals](https://modrinth.com/mod/applied-energistics-2-wireless-terminals) | >= v1.4 |
| [AppliedE](https://modrinth.com/mod/appliede) | >= v1.4 |
| [Feathers](https://modrinth.com/mod/feathers) | >= v1.4 |
| [Inventory Sorting](https://modrinth.com/mod/inventory-sorting) | >= v1.4 |
| [ME Requester](https://modrinth.com/mod/merequester) | >= v1.4 |
| [ProjectE](https://www.curseforge.com/minecraft/mc-mods/projecte) | >= v1.4 |
| [Scout](https://modrinth.com/mod/scout) | >= v1.4 |
| [Sawmill](https://modrinth.com/mod/universal-sawmill) | >= v1.5 |
| [Tierify](https://www.curseforge.com/minecraft/mc-mods/tierify) | >= v1.5 |
| [Beans Backpacks](https://modrinth.com/mod/beans-backpacks-3) | >= v1.6 |

</details>

### Recommendations

- **[Raised](https://modrinth.com/mod/raised) Mod**: Moves the hotbar so it doesn't crop the texture;
- **[Grass Block Plugin for NBTpack](https://modrinth.com/resourcepack/nbtpack-grass) Resource Pack**: Tilts item displays so that items that have long textures (swords for example) does't look off with the rounded item slots.

### Minecraft Pack Version Compatibility
[Minecraft pack format](https://minecraft.wiki/w/Pack_format)

| Minecraft Pack | Cobblemon Interface |
|---------------:|:--------------------|
| 9              | <= v0.7             |
| 15             | >= v0.8             |
| 34             | >= v1.6             |

### Included
This project includes a modified version of the [cobblemon tooltips](https://modrinth.com/resourcepack/poketips) resourcepack.

[![Banner](https://api.mcbanners.com/banner/saved/LZrReFoiohJHWs.png)](https://modrinth.com/resourcepack/poketips)

---

[![Ko-fi](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/donate/kofi-singular_vector.svg)](https://ko-fi.com/M4M5K4BDR)

[![Discord](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3.1.0/assets/compact/social/discord-singular_vector.svg)](https://discord.com/channels/934267676354834442/1091579923396820992) on the [Official Cobblemon Discord Server](https://discord.gg/cobblemon)