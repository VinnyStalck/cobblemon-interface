# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Fixed

- Updated [Waystones](https://modrinth.com/mod/waystones) mod gui texture name.


## [1.7.0] - 2025-01-07

### Changed

- Updated Beans Backpacks textures.

### Removed

- Apotheosis and Zenith support.


## [1.6.0] - 2024-12-16

**Minecraft 1.21 Update**

Adapted textures for the changes made in Minecraft 1.21.

### Added

- [Beans Backpacks](https://modrinth.com/mod/beans-backpacks-3) mod compat.

### Fixed

- Updated pack format to 34.


### v1.5.0 - Hotbar Fix - 2024-07-16

Fixed the misalignment on the hotbar icons and added support for [Sawmill](https://modrinth.com/mod/universal-sawmill), [Tierify](https://www.curseforge.com/minecraft/mc-mods/tierify) mods.

**Additions:**
- Support for [Sawmill](https://modrinth.com/mod/universal-sawmill), [Tierify](https://www.curseforge.com/minecraft/mc-mods/tierify) mods;
- Added missing icons from [Farmer's Delight](https://modrinth.com/mod/farmers-delight) and [Farmer's Delight Refabricated](https://modrinth.com/mod/farmers-delight-refabricated).

**Fixes**
- Fixed the misalignment on the hotbar health/armor/mount health/baubles icons from [AppleSkin](https://modrinth.com/mod/appleskin), [Dehydration](https://modrinth.com/mod/dehydration), [Feathers](https://modrinth.com/mod/feathers), [Overflowing Bars](https://modrinth.com/mod/overflowing-bars).


### v1.4.0 - Inventory Tech - 2024-06-13

Added support for a lot of tech and inventory mods requested by people in discord.

**Additions:**
- Support for [Applied Energistics 2](https://modrinth.com/mod/ae2), [AppliedE](https://modrinth.com/mod/appliede), [Applied Energistics 2 Wireless Terminals](https://modrinth.com/mod/applied-energistics-2-wireless-terminals), [ME Requester](https://modrinth.com/mod/merequester) and [ProjectE](https://www.curseforge.com/minecraft/mc-mods/projecte) by @TorchTheDragon (`@torchthedragon` on Discord).
- Support for [Feathers](https://modrinth.com/mod/feathers), [Inventory Sorting](https://modrinth.com/mod/inventory-sorting) and [Scout](https://modrinth.com/mod/scout).

**Fixes**
- [REI](https://modrinth.com/mod/rei) small buttons looking weird.


### v1.3.0 - REI and Traveler's Backpack - 2024-06-05

Added support for [REI](https://modrinth.com/mod/rei) and [Traveler's Backpack](https://modrinth.com/mod/travelersbackpack) mods and updated [Curios API](https://modrinth.com/mod/curios) textures.

**Additions:**
- Support for [Roughly Enough Items (REI)](https://modrinth.com/mod/rei) and [Traveler's Backpack](https://modrinth.com/mod/travelersbackpack) mods.

**Changes**
- Updated [Curios API](https://modrinth.com/mod/curios) support to have a inventory revamp texture.


### v1.2.0 - Inmis and Inventorio - 2024-04-19

Added support for [Inmis](https://modrinth.com/mod/inmis) and [Inventorio](https://modrinth.com/mod/inventorio) mods.

**Additions:**
- Support for [Inmis](https://modrinth.com/mod/inmis) and [Inventorio](https://modrinth.com/mod/inventorio) mods by `@533k` (Seek) on Discord.


### v1.1.0 - More Mods - 2024-04-12

Added support for [Expanded Storage](https://modrinth.com/mod/expanded-storage), [Trinkets](https://modrinth.com/mod/trinkets) and [Iron Furnaces](https://modrinth.com/mod/iron-furnaces) mods.

**Changes**
- Changed vanilla inventory texture to have space for slots added by mods.

**Additions:**
- Support for [Expanded Storage](https://modrinth.com/mod/expanded-storage) and [Trinkets](https://modrinth.com/mod/trinkets) mod;
- Support for [Iron Furnaces](https://modrinth.com/mod/iron-furnaces) mod by `@torchthedragon` on Discord.


### v1.0.0 - Release

Create support and some fixes.

**Additions:**
- Support for [Create](https://modrinth.com/mod/create) and [Create Fabric](https://modrinth.com/mod/create-fabric) mods. By: `@torchthedragon` on Discord.

**Fixes**
- Waystones mod checkbox;
- Tom's Simple Storage mod terminals;
- Sophisticated Core mod gui controls.