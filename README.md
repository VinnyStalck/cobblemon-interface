[![Ko-fi](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/donate/kofi-singular_vector.svg)](https://ko-fi.com/M4M5K4BDR)

[![Discord](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3.1.0/assets/compact/social/discord-singular_vector.svg)](https://discord.com/channels/934267676354834442/1091579923396820992) on the [Official Cobblemon Discord Server](https://discord.gg/cobblemon)

---

## Cobblemon Interface

Makes Minecraft's interface and sounds fit the [Cobblemon](https://modrinth.com/mod/cobblemon) mod aesthetics.

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/resourcepack/cobblemon-interface)
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://curseforge.com/minecraft/texture-packs/cobblemon-interface)

## Cobblemon Interface: Modded

A complementary resourcepack to [Cobblemon Interface](https://modrinth.com/project/cobblemon-interface) that makes many mods interface and sounds fit the [Cobblemon](https://modrinth.com/mod/cobblemon) mod aesthetics.

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/resourcepack/cobblemon-interface-modded)
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://curseforge.com/minecraft/texture-packs/cobblemon-interface-modded)

