# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.0] - 2024-12-16

**Minecraft 1.21 Update**

Adapted textures for the changes made in Minecraft 1.21.

### Added

- All empty slots textures.


### v1.4.0-SNAPSHOT - Minecraft 1.21 Update - 2024-11-05

Adapted textures for the changes made in Minecraft 1.21.

**Changes**
- Changed Wither Heart icons;
- Updated Advancement sprites;
- Updated Recipe Book UI;
- Tweaked Inventory large effect background;

**Fixes**
- Fixed Grindstone and Villager container texture.
- Updated pack format to 34.


### v1.3.0 - Hotbar Fix - 2024-07-16

Fixed the misalignment on the hotbar icons.

**Fixes**
- Fixed the misalignment on the hotbar health/armor/mount health/baubles icons.


### v1.2.0 - Bundle and Gamemode Switcher - 2024-06-16

Added two missing textures that weren't included before.

**Additions:**
- Textures for the Bundle GUI and Gamemode Switcher GUI.


### v1.1.1 - Audible GUI

Made Minecraft's menus click sound a bit louder.

**Changes:**
- Increased click_stereo.ogg file audio gain by ~20dB.


### v1.1.0 - Starter Selection

Updated Cobblemon's Starter Selection screen to fit the newer UI style.

**Additions:**
- Textures for Cobblemon's Starter Selection screen;

**Fixes:**
- Increased in one pixel the crafting space in the inventory.


### v1.0.0 - Release

Removed black background bars on inventory names to help with consistency and mod support.

**Changes:**
- Moved mod support to [Cobblemon Interface: Modded](https://modrinth.com/resourcepack/cobblemon-interface-modded);
- Remove Black bar on inventory textures;
- Changed all inventories to not have cut parts.