# Cobblemon Interface: Recolor

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/resourcepack/cobblemon-interface-recolored)
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://curseforge.com/minecraft/texture-packs/cobblemon-interface-recolored)

---

An optional addon to [Cobblemon Interface](https://modrinth.com/project/cobblemon-interface) resourcepack that changes the health and experience bars to pair with the colors used in Pokémon's HP and EXP in [Cobblemon](https://modrinth.com/mod/cobblemon).

## Comparison

![](../assets/images/recolored/comparison.png)

## Matching Colors

![](../assets/images/recolored/matching-colors.png)

---

[![Ko-fi](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/donate/kofi-singular_vector.svg)](https://ko-fi.com/M4M5K4BDR)

[![Discord](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3.1.0/assets/compact/social/discord-singular_vector.svg)](https://discord.com/channels/934267676354834442/1091579923396820992) at the [Official Cobblemon Discord Server](https://discord.gg/cobblemon)