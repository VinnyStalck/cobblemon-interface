# Changelog

### b0.10 - Consistency

Removed black background bars on inventory names to help with consistency and mod support.

**Changes:**
- Remove Black bar on inventory textures;

**Additions:**
- Support for [Create](https://modrinth.com/mod/create) and [Create Fabric](https://modrinth.com/mod/create-fabric) mods. By: `@torchthedragon` on Discord;

**Fixes**
- Waystones mod checkbox;
- Tom's Simple Storage mod terminals;
- Sophisticated Core mod gui controls.

### b0.9.1 - Bit More Compatibilities - 2024-03-14

Added compatibility to a few other mods.

**Additions:**
- Support for [Simple Voice Chat](https://modrinth.com/plugin/simple-voice-chat), [Tom's Simple Storage](https://modrinth.com/mod/toms-storage), [Waystones](https://modrinth.com/mod/waystones) and [Zenith](https://modrinth.com/mod/zenith) mods. By: `@torchthedragon` on Discord.

**Fixes**
- Added missing sophisticated core mod scroll bar texture.


### b0.9.0 - Too Many Compatibilities - 2024-03-13

Added compatibility to many different mods

**Additions:**
- Support for [Automobility](https://modrinth.com/mod/automobility), [Better Advancements](https://modrinth.com/mod/better-advancements), [Cobblemon Integrations](https://modrinth.com/mod/cobblemon-integrations), [Cooking for Blockheads](https://modrinth.com/mod/cooking-for-blockheads), [Cosmetic Armor Reworked](https://minecraft.curseforge.com/projects/cosmetic-armor-reworked), [Curios API](https://modrinth.com/mod/curios), [Easy Villagers](https://modrinth.com/mod/easy-villagers), [Farmer's Delight](https://modrinth.com/mod/farmers-delight), [Farmer's Delight [Fabric]](https://modrinth.com/mod/farmers-delight-fabric), [Realistic Horse Genetics](https://modrinth.com/mod/realistic-horse-genetics), [Iron Chests](https://modrinth.com/mod/iron-chests), [Just Enough Items](https://modrinth.com/mod/jei), [Just Enough Resources](https://modrinth.com/mod/just-enough-resources-jer), [Just Enough Professions](https://modrinth.com/mod/just-enough-professions-jep), [Little Logistics](https://modrinth.com/mod/little-logistics), [PokeFood](https://modrinth.com/mod/pokefood), [Polymorph](https://modrinth.com/mod/polymorph), [Rechiseled](https://modrinth.com/mod/rechiseled), [Sophisticated Core](https://www.curseforge.com/minecraft/mc-mods/sophisticated-core) and [Xaero's Minimap (Fair-Play)](https://modrinth.com/mod/xaeros-minimap-fair) mods. By: `@mnesikos` on Discord;
- Support for [Apotheosis](https://www.curseforge.com/minecraft/mc-mods/apotheosis), [EMI](https://modrinth.com/mod/emi), [Sophisticated Core](https://www.curseforge.com/minecraft/mc-mods/sophisticated-core), [Sophisticated Storage](https://www.curseforge.com/minecraft/mc-mods/sophisticated-storage), [Sophisticated Backpacks](https://www.curseforge.com/minecraft/mc-mods/sophisticated-backpacks) mods. By: `@torchthedragon` on Discord;
- Horse, Villager and Legacy Smithing containers and other small gui elements. By: `@mnesikos` on Discord.


### b0.8.4 - Trash Click Sound - 2023-12-04

**Additions:**
- Support for [TrashSlot](https://modrinth.com/mod/trashslot) mod;

**Changes:**
- Changed the click sound to the one used in the menu of the Pokémon Scarlet/Violet games.

### b0.8.3 - Dehydration and Tooltips Support - 2023-11-20

**Additions:**
- Support for [Dehydration](https://modrinth.com/mod/dehydration) mod;
- Support for [Legendary Tooltips](https://modrinth.com/mod/legendary-tooltips) mod and included the [Cobblemon Tooltips](https://modrinth.com/resourcepack/poketips) resourcepack.

**Changes:**
- Moved armor bar 1 pixel up.

**Fixes:**
- Overfowing Bars' half toughtness bar icon beeing on the wrong side.

### b0.8.2 - All Vanilla Containers - 2023-11-08

**Additions:**
- Container textures for *Beacon*, *Stonecutter*, *Hopper*, *Grindstone*, *Loom* and *Smithing Table*.

### b0.8.1 - Containers & Overflowing Bars - 2023-11-01

**Additions:**
- Container textures for *Anvil*, *Brewing Stand*, *Cartography Table*, *Dispenser* and *Enchanting table*;
- Support for [Overflowing Bars](https://modrinth.com/mod/overflowing-bars) mod.

### b0.8 - Friends and Farms Update

Updated to Minecraft 1.20.1

**Changes:**
- Pack format changed to 15;
- Creative inventory tabs texture updated;
- Fabric creative buttons texture updated.